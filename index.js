const {Builder, By, Key, until, WebElement} = require('selenium-webdriver');
var capabilities = {
    browserName: 'chrome',
    "goog:chromeOptions": {
        args: ["headless"],
        mobileEmulation: {
            "deviceMetrics": {
                 "width": 360,
                  "height": 640,
                  "pixelRatio": 3
             },
             "userAgent": "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
        }
    }
};



async function runUserJourney() {
  const isDone = false;
    let driver = await new Builder().withCapabilities(capabilities).build();
    const quit = () => {
      driver.quit();
    }
    console.log('startou?');
    try {
      await driver.get('http://localhost:5000');
      await driver.wait(until.titleIs('brizicam-app'));
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'English')]"))).click();
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'Start')]"))).click();
      await driver.wait(until.elementLocated(By.id("tos"))).sendKeys('webdriver', Key.SPACE);
      await driver.wait(until.elementLocated(By.id("privacy"))).sendKeys('webdriver', Key.SPACE);
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'I ACCEPT')]"))).click();
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'stand')]"))).click();
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'LANDMARK')]"))).click();
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'Next')]"))).click();
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'1')]"))).click();
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'Next')]"))).click();
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'1')]"))).click();
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'Next')]"))).click();
      await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'Next')]"))).click();
      const cameraButton = await driver.wait(until.elementLocated(By.id("cameraButton")));
      if(cameraButton.isDisplayed() && cameraButton.isEnabled()) {
        setTimeout(async () => {
          await driver.wait(until.elementLocated(By.id("cameraButton"))).click();
          await driver.wait(until.elementLocated(By.xpath("//img[contains(@alt,'snapshot 0')]"))).click();
          setTimeout(async () => {
            await driver.wait(until.elementLocated(By.xpath("//button[contains(.,'Continue')]"))).click();
            setTimeout(async() => {
              driver.quit();
              runUserJourney();
            },5000);
          }, 5000);
        }, 5000);
      }
    }  catch (e) {
      console.log(e);
      driver.quit();
    }
  };

  runUserJourney();
  // runUserJourney();
